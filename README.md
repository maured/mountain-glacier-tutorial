# Mountain Glacier Modelling using Elmer/Ice

This repository contains the material in order to model the dynamics of a set of mountain glaciers within Central Europe (Data from other regions are not included)

It can be seen as workflow basis for using Elmer/Ice on continental glaciers.

<div class="centered">
<img src="data/figures/EI_example.png" width="600px" alt="EI_example"> 
</div>

# Content

- **data/**: required materials (Surface DEMs from ASTER dataset, RGI shapefiles (v6) and inverted glaciers thickness rasters from Farinotti & al. 2019 )

- **steps/**: Scripts for the whole workflow, preprocessing & elmer run

- **wdirs/**: directory where the necessary data is put for elmer to run

- **sif_extra_content/**: directory of required Solvers and files for Elmer


# Tutorial

- **Step by step tutorial**:
In order to start, go into **steps/** to begin the 3D modelling of your favourite glacier. You will run every step one after the other, to better understand the workflow of a simulation.


- **"I don't want the details" way**:
In order to process all the steps at once for a diagnostic run, simply launch *all_step.sh*:
	```shell
	./all_steps.sh
	```
You can choose wich glacier you want to model by changing the file *steps/glacierlist.txt* and adding/deleting lines of RGIId (format *RGI60.11.#####*).



