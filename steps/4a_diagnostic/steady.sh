#=========================================
# AUTHOR:   D. MAURE
# ORGANIZATION: IGE(CNRS-France)
#
# VERSION: V1 
# CREATED: 2021-04
# MODIFIED: 
#  * 2021-07-26: Added header
#
#========================================== 
############################
#Header for functions calculating max & min of netcdf + directories variables

cd ../..

HOME_DIR=$(pwd)
data_path=$HOME_DIR/data
glacierlist=$HOME_DIR/steps/glacierlist.txt
cd $HOME_DIR
#############################


while read line;do	
	echo "$line"
	cd wdirs/$line

	cp ../../steps/4a_diagnostic/steady.sif steady.sif

	##
	ElmerSolver steady.sif
	##

	cd $HOME_DIR
done <$glacierlist
