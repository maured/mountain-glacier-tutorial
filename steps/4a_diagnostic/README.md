# Initialisation of Elmer run

# Description

We've now see that our mesh is fully extruded. We can now use the finite element model to solve for different equations of interest.

We can, for example, calculate the ice velocity field given by Glen's flow law, by calling a solver on our mesh.

After putting *steady.sif* in a given glacier directory, simply call 
	```shell
	ElmerSolver steady.sif
	```

to calculate the velocity fields of the glacier.

To exectute automatically on every glacier directory, simply run: 
	```shell
	./init.sh
	```
from this current reposit.

# Velocity field visualisation with paraview

As in the previous step, the results of the siulation can be read by paraview by opening *wdirs/RGI60.11.#####/OUTPUT/RUN_1*
