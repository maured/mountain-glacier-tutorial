# Mountain Glacier Modelling using Elmer/Ice
This directory contains the core of the tutorial. Every script has to be executed from its "step" directory.

You can select what glacier you want to process by adding/deleting rows of RGIIds in *glacierlist.txt*.
By default, the only glacier processed is *Glacier d'Argentière* in Mt Blanc massif.

Here are a few other examples you can try:
- Mer de Glace: RGI60-11.03643
- Aletschglacier: RGI60-11.01450
- Hintereisferner: RGI60-11.00897

(keep in mind that Elmer computations can be very long if your computer isn't fast enought. It that is the case, try a smaller glacier first )



# Workflow

- **1-DEM process**: The first step is to define what is going to be the bottom and top surface of our glacier (and surrounding terrain in the case of a prognostic run!).

- **2-Meshing**: Using the extent of the terrain defined in step *1*, we need to create a 2D mesh with the corresponding extent, understandable for Elmer.

- **3-Elmer Initialisation**: Once our mesh is done, we need Elmer to extrapolate the 2D mesh into a 3D one, using the DEMs we derived in step *1*. In a "real" run, step 3 and 4a/b can be done using the same *.sif* file.

- **4a-Diagnostic Run**: Eventually, we tell our model to solve the equations we want: we will calculate the ice velocity as given by Glen's flow law, with a no slip basal condition. The results can easily be looked at in paraview.

- **4b-Prognostic Run**: TODO
