# Initialisation of Elmer run

# Description

We are now ready to launch the first step of an Elmer/Ice run.
For the moment, the mesh we created is still a flat 2D plane. The *INIT.sif* file gives some instructions to ElmerIce for it to extrude the mesh properly, using the given DEMs we prescribed.


The files in *sif_extras/* are also used to define some parameters (rehology & other constant), aswell as to give instructions to Elmer.

All files (*INIT.sif* and *sif_extras/* need to be coopied into a glacier directory before the run.

When all the necessary files are in a glacier directory, Elmer is called by simply running

	```shell
	ElmerSolver INIT.sif
	```
in the glacier directory.

To do all the tasks at one, run *init.sh*:
	```shell
	./init.sh
	```
from this current reposit.

# 3D Mesh Visualisation

In order to check that the process worked until this point, we can look at the 3D mesh extruded by Elmer in *paraview*. Simply open *wdirs/RGI60.11.#####/OUTPUT/RUN_0.vtu* in paraview to look at the mesh.

