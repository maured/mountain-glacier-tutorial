#=========================================
# AUTHOR:   D. MAURE
# ORGANIZATION: IGE(CNRS-France)
#
# VERSION: V1 
# CREATED: 2021-04
# MODIFIED: 
#  * 2021-07-26: Added header
#
#========================================== 

############################
#Header for functions calculating max & min of netcdf + directories variables
function ncmin { ncap2 -O -C -v -s "foo=${1}.min();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }
function ncmax { ncap2 -O -C -v -s "foo=${1}.max();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }

cd ../..

HOME_DIR=$(pwd)
data_path=$HOME_DIR/data
aster_data=$data_path/surface_DEM/CentralEurope.tif
#############################







#The process will be done for every glacier in the list (identified by their RGIId)
glacierlist=$HOME_DIR/steps/glacierlist.txt
cd $HOME_DIR

while read line;do	
	echo "$line"
	mkdir wdirs/$line
	
	#we begin by making the directory structure for elmer 
	mkdir wdirs/$line/INPUT
	mkdir wdirs/$line/OUTPUT

	cd wdirs/$line

#########THICKNESS DATA IMPORT
	#we then import the thickness dem from the dataset of Farinotti & al. 2019
	gdal_translate -of netcdf $data_path/thickness_DEMs/thickness_${line}.tif INPUT/thick.nc
	
#########SURFACE DATA IMPORT
	#we then warp the ASTERv3 surface dataset on the thickness raster
	cs=25 #this is the resolution of the thickness data
	xmin0=$(ncmin x INPUT/thick.nc)
	xmax0=$(ncmax x INPUT/thick.nc)
	ymin0=$(ncmin y INPUT/thick.nc)
	ymax0=$(ncmax y INPUT/thick.nc)

	xmin=$(echo "scale=10; $xmin0-$cs/2" | bc -l)
	ymin=$(echo "scale=10; $ymin0-$cs/2" | bc -l)
	xmax=$(echo "scale=10; $xmax0+$cs/2" | bc -l)
	ymax=$(echo "scale=10; $ymax0+$cs/2" | bc -l)

	echo $xmin $xmax $ymin $ymax
	
	gdalwarp -ot Float32 -s_srs EPSG:4326 -t_srs EPSG:32632 -r bilinear -of GTiff -tr $cs $cs -te $xmin $ymin $xmax $ymax -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -wo OPTIMIZE_SIZE=TRUE $aster_data INPUT/surface.tif
	gdal_translate -of netcdf INPUT/surface.tif INPUT/surface.nc

#########FINAL DEM PROCESSING
	#eventually we merge the files into one contaning surface&bedrock DEMs.
    	ncrename -v Band1,surface INPUT/surface.nc
	ncrename -v Band1,thick  INPUT/thick.nc
	
	ncks -A INPUT/thick.nc INPUT/surface.nc
	ncap2 -s "bed=surface-thick" INPUT/surface.nc INPUT/DEMs.nc

	cd $HOME_DIR
done < $glacierlist

