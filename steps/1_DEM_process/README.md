# Import & process the necessary DEMs for the simulation

## Description
### Thickness inversion data
We use the dataset of inverted glacier thickness from [Farinotti & al. 2019](https://doi.org/10.1038/s41561-019-0300-3) as a start for our simulation. Different thickness estimates were calculated using a set of 5 inversion models. We will use the composite rasters (average of different inversion models) for our simulations (see in /data)

### Surface DEMs
For the tutorial, a composite of tiles from ASTER Global Digital Elevation Model V003 will be used, with a resolution of 30mx30m. This dataset along with others can be downloaded from [NASA's EARTHDATA](https://search.earthdata.nasa.gov/search/) website.

### Projection details
We will use ***gdal*** and ***nco*** for reprojection and conversions from GeoTIFF to NetCDF files.
Two functions aliases are declared in *DEMs_import.sh* using *nco*: ncmin and ncmax, used in order to get the extent of a given raster file.



**COORDINATE SYSTEM WARNING:** In the western part of the alps, the corresponding CRS to map the surface to is *ESPG 32632*. However, if you want to choose glaciers from the Eastern part of the region (e.g Austria), the CRS of the thickness datasets will be *ESPG 32633*. You can change this in line 
53 of *DEMs_import.sh*.



