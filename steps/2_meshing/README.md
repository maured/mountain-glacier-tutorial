# Mesh creation for Elmer/Ice

# Description

Starting back from the DEMs we processed, we will create a rectangular mesh matching their extent.

The file *rectangle.geo* is a file that can be read by *gmsh* and whose parameters(x/y min/max) can be change via the command *-set value* (see in makemesh.sh)

Once a *.msh* file is created, with a given resolution value (by default the resolution is 100m here), we can call ElmerGrid in order to convert it to a *.mesh*, understandable by Elmer.
