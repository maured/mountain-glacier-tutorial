#=========================================
# AUTHOR:   D. MAURE
# ORGANIZATION: IGE(CNRS-France)
#
# VERSION: V1 
# CREATED: 2021-04
# MODIFIED: 
#  * 2021-07-26: Added header
#
#========================================== 
############################
#Header for functions calculating max & min of netcdf + directories variables
function ncmin { ncap2 -O -C -v -s "foo=${1}.min();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }
function ncmax { ncap2 -O -C -v -s "foo=${1}.max();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }

cd ../..

HOME_DIR=$(pwd)
data_path=$HOME_DIR/data
#############################







#The process will be done for every glacier in the list (identified by their RGIId)
glacierlist=$HOME_DIR/steps/glacierlist.txt
cd $HOME_DIR

while read line;do	
	echo "$line"
	cd wdirs/$line

	#we first get the extent of the raster files
	xmin=$(ncmin x INPUT/DEMs.nc)
	xmax=$(ncmax x INPUT/DEMs.nc)
	ymin=$(ncmin y INPUT/DEMs.nc)
	ymax=$(ncmax y INPUT/DEMs.nc)

	res=100.0 #this is the wanted resolution of the mesh (to be 

	echo $xmin $xmax $ymin $ymax
	
	#We paste the reference rectangle.geo into the glacier directory
	cp $HOME_DIR/steps/2_meshing/rectangle.geo INPUT/rectangle.geo

	#gmsh converts the .geo file into a .msh
	gmsh -1 -2 INPUT/rectangle.geo -setnumber xmin $xmin -setnumber xmax $xmax \
			         -setnumber ymin $ymin -setnumber ymax $ymax \
				 -setnumber lc $res 

	#Finally we cal ElmerGrid to convert the .msh into a readable file for Elmer
	ElmerGrid 14 2 INPUT/rectangle.msh -autoclean
	ElmerGrid 2 5 INPUT/rectangle

	cd $HOME_DIR
done < $glacierlist

