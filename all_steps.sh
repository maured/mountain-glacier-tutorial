#This script will run every steps at once.
cd steps
echo "Executing step 1"
cd 1_DEM_process
./DEMs_import.sh
cd ..

echo "Executing step 2"
cd 2_meshing
./makemesh.sh
cd ..

echo "Executing step 3"
cd 3_elmer_initialisation
./init.sh
cd ..

echo "Executing step 4a"
cd 4a_diagnostic
./steady.sh
cd ..

echo "Simulation finished!"
